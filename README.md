
# P8_code

Le projet TourGuide permet aux utilisateurs de voir quelles sont les attractions touristiques à proximité et d’obtenir des réductions sur les séjours d’hôtel ainsi que sur les billets de différents spectacles.

Or, depuis la publication d’un article, l’application connait une forte croissante. L’application n’est pas prévue pour gérer un trop grand nombre de connexions simultanées. Les clients ont remonté des bugs qui ont permis une perte des clients.

Et donc, je l'ai amélioré. Les bugs remontés sont corrigés et j'ai modifié l'architecture. C'est beaucoup plus performant.


***

![La nouvelle architecture](doc/new_architecture.png)
La nouvelle architecture

***


## Document

- [Le document fonctionnelle et technique](doc/Document%20fonctionnelle%20et%20technique.docx)
- [Le rapport des performances](doc/TourGuide+Performance+Graphs.xlsx)

***

## Installation
Il existe deux possibilités :
- Soit en utilisant directement le jar ( disponible dans l'artifact)
- Soit en utilisant le docker. Il suffit de télécharger le projet et lancer la commande suivante : 
```
docker-compose up -d
```

## Usage

La liste des endpoints
```
----- tour guide -----

// page accueil
http://localhost:9005/ (GET)

// afficher mon localisation
http://localhost:9005/getLocation (GET)

// afficher tous les actractions proximités
http://localhost:9005/getNearbyAttractions (GET)

// afficher les récompenses
http://localhost:9005/getRewards (GET)

// afficher tous les localisations de tous les utilisateurs
http://localhost:9005/getAllCurrentLocations (GET)

// afficher les promotions
http://localhost:9005/getTripDeals (GET)


----- user -----

// afficher les préférences d'un user
http://localhost:9005/user/preference (GET)

// mise à jour des préférences d'un user
http://localhost:9005/user/preference (PUT)


----- tracker -----

// lancer le traqueur
http://localhost:9005/tracker/start (GET)

// arrêter le traqeur
http://localhost:9005/tracker/stop (GET)
```