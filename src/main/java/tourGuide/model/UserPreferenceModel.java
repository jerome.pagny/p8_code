package tourGuide.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.javamoney.moneta.Money;
import tourGuide.dto.UserPreferenceDTO;

import javax.money.CurrencyUnit;
import javax.money.Monetary;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPreferenceModel {

    private int attractionProximity = Integer.MAX_VALUE;
    private CurrencyUnit currency = Monetary.getCurrency("USD");
    private Money lowerPricePoint = Money.of(0, currency);
    private Money highPricePoint = Money.of(Integer.MAX_VALUE, currency);
    private int tripDuration = 1;
    private int ticketQuantity = 1;
    private int numberOfAdults = 1;
    private int numberOfChildren = 0;

    public UserPreferenceModel(UserPreferenceDTO userPreferenceDTO){
        this.attractionProximity = userPreferenceDTO.getAttractionProximity();
        this.currency = Monetary.getCurrency(userPreferenceDTO.getCurrency());
        this.lowerPricePoint = Money.of(userPreferenceDTO.getLowerPricePoint(), currency);
        this.highPricePoint = Money.of(userPreferenceDTO.getHighPricePoint(), currency);
        this.tripDuration = userPreferenceDTO.getTripDuration();
        this.ticketQuantity = userPreferenceDTO.getTicketQuantity();
        this.numberOfAdults = userPreferenceDTO.getNumberOfAdults();
        this.numberOfChildren = userPreferenceDTO.getNumberOfChildren();
    }


}
