package tourGuide.model;

import gpsUtil.location.VisitedLocation;
import lombok.Data;
import tripPricer.Provider;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
public class UserModel {

    private final UUID userId;
    private final String userName;
    private String phoneNumber;
    private String emailAddress;
    private Date latestLocationTimestamp;
    private List<VisitedLocation> visitedLocations = new ArrayList<>();
    private List<UserRewardModel> userRewardModels = new ArrayList<>();
    private UserPreferenceModel userPreferenceModel = new UserPreferenceModel();
    private List<Provider> tripDeals = new ArrayList<>();

    public UserModel(UUID userId, String userName, String phoneNumber, String emailAddress) {
        this.userId = userId;
        this.userName = userName;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
    }

    public void addUserReward(UserRewardModel userRewardModel) {
        if (!userRewardModels.contains(userRewardModel)) {
            userRewardModels.add(userRewardModel);
        }
    }

    public VisitedLocation getLastVisitedLocation() {
        return visitedLocations.get(visitedLocations.size() - 1);
    }

    public void addToVisitedLocations(VisitedLocation visitedLocation) {
        visitedLocations.add(visitedLocation);
    }


}
