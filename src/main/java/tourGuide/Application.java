package tourGuide;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import tourGuide.config.BuildUserInternalTesting;
import tourGuide.model.UserTestingModel;

import java.util.HashMap;
import java.util.Locale;

@SpringBootApplication
public class Application {

    private final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        SpringApplication.run(Application.class, args);
    }

    @Value("${test.mode}")
    private boolean testMode;

    @Bean
    public UserTestingModel buildDataTesting() {

        UserTestingModel buildData;
        if (testMode) {
            LOGGER.info("TestMode enabled");
            LOGGER.debug("Initializing users");
            buildData = BuildUserInternalTesting.build();
            LOGGER.debug("Finished initializing users");
        } else {
            buildData = new UserTestingModel(new HashMap<>());
        }

        return buildData;
    }

}
