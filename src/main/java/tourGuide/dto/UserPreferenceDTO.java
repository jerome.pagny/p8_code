package tourGuide.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import tourGuide.model.UserPreferenceModel;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPreferenceDTO {

    private int attractionProximity;
    private String currency;
    private int lowerPricePoint;
    private int highPricePoint;
    private int tripDuration;
    private int ticketQuantity;
    private int numberOfAdults;
    private int numberOfChildren;


    public UserPreferenceDTO(UserPreferenceModel user){
        this.attractionProximity = user.getAttractionProximity();
        this.currency = user.getCurrency().getCurrencyCode();
        this.lowerPricePoint = user.getLowerPricePoint().getContext().getPrecision();
        this.highPricePoint =  user.getLowerPricePoint().getContext().getMaxScale();
        this.tripDuration = user.getTripDuration();
        this.ticketQuantity = user.getTicketQuantity();
        this.numberOfChildren = user.getNumberOfChildren();
        this.numberOfAdults = user.getNumberOfAdults();
    }


}
