package tourGuide.dto;

import gpsUtil.location.Location;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class UserLocationDTO {

    private String uuid;
    private Location location;

}
