package tourGuide.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import tourGuide.service.runnable.TrackerRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Service
@AllArgsConstructor
public class TrackerService {

    private final TourGuideService tourGuideService;
    private final UserService userService;
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private final List<Future<?>> futures = new ArrayList<>();

    public void startTracking() {

        if (futures.size() == 0) {
            Future<?> future = executorService.submit(new TrackerRunnable(tourGuideService, userService));
            futures.add(future);
        }
    }

    public void stopTracking() {
        futures.get(0).cancel(true);
        futures.clear();
    }


}
