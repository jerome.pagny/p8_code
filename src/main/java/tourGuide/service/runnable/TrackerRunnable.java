package tourGuide.service.runnable;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tourGuide.constant.Config;
import tourGuide.model.UserModel;
import tourGuide.service.TourGuideService;
import tourGuide.service.UserService;

import java.util.List;
import java.util.concurrent.TimeUnit;

@AllArgsConstructor
public class TrackerRunnable implements Runnable {

    private final Logger LOGGER = LoggerFactory.getLogger(TrackerRunnable.class);
    private final TourGuideService tourGuideService;
    private final UserService userService;

    @SneakyThrows
    @Override
    public void run() {

        StopWatch stopWatch = new StopWatch();

        while (true) {

            List<UserModel> users = userService.getAll();

            LOGGER.debug("Begin Tracker. Tracking " + users.size() + " users.");

            stopWatch.start();
            tourGuideService.buildUserTrackThreads(users);
            stopWatch.stop();

            LOGGER.debug("Tracker Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");

            stopWatch.reset();

            try {
                LOGGER.debug("Tracker sleeping");
                TimeUnit.SECONDS.sleep(Config.TRACKING_POLLING_INTERVAL);
            } catch (InterruptedException e) {
                break;
            }
        }

    }

}
