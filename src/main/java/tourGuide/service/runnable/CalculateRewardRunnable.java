package tourGuide.service.runnable;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tourGuide.model.UserModel;
import tourGuide.service.RewardsService;

@AllArgsConstructor
public class CalculateRewardRunnable implements Runnable{

    private final Logger LOGGER = LoggerFactory.getLogger(CalculateRewardRunnable.class);
    private final RewardsService rewardsService;
    private final UserModel user;

    @SneakyThrows
    @Override
    public void run() {
        LOGGER.debug("START TO CALCULATE : " + user.getUserId());
        rewardsService.calculateRewards(user);
        LOGGER.debug("END TO CALCULATE : " + user.getUserId());
    }
}
