package tourGuide.service.runnable;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tourGuide.model.UserModel;
import tourGuide.service.TourGuideService;

@AllArgsConstructor
public class TrackUserLocationRunnable implements Runnable{

    private final Logger LOGGER = LoggerFactory.getLogger(TourGuideService.class);
    private final TourGuideService tourGuideService;
    private final UserModel userModel;

    @SneakyThrows
    @Override
    public void run() {
        LOGGER.debug("START TO TRACK USER LOCATION : " + userModel.getUserId());
        tourGuideService.trackUserLocation(userModel);
        LOGGER.debug("END TO TRACK USER LOCATION : " + userModel.getUserId());
    }
}
