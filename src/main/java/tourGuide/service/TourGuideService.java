package tourGuide.service;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tourGuide.dto.NearByAttractionDTO;
import tourGuide.model.UserModel;
import tourGuide.model.UserRewardModel;
import tourGuide.service.runnable.TrackUserLocationRunnable;
import tripPricer.Provider;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Service
public class TourGuideService {

    private final Logger LOGGER = LoggerFactory.getLogger(TourGuideService.class);
    private final RewardsService rewardsService;

    private final GpsUtilService gpsUtilService;

    private final TripPricerService tripPricerService;

    private final ExecutorService executorService = Executors.newFixedThreadPool(100);
    private final List<Future<?>> futures = new ArrayList<>();

    public TourGuideService(GpsUtilService gpsUtilService,
                            RewardsService rewardsService,
                            TripPricerService tripPricerService) {
        this.gpsUtilService = gpsUtilService;
        this.rewardsService = rewardsService;
        this.tripPricerService = tripPricerService;

    }

    /**
     * Get all user rewards
     *
     * @param user the user model
     * @return a list of user reward model
     */
    public List<UserRewardModel> getUserRewards(UserModel user) {
        return user.getUserRewardModels();
    }

    /**
     * Get the user location
     *
     * @param user the user model
     * @return the visited location
     */
    public VisitedLocation getUserLocation(UserModel user) {
        return (user.getVisitedLocations().size() > 0) ?
                user.getLastVisitedLocation() :
                trackUserLocation(user);
    }

    /**
     * Get a list of trip deals according to the user preference
     *
     * @param user the user model
     * @return a list of provider
     */
    public List<Provider> getTripDeals(UserModel user) {
        int cumulativeRewardPoints = user.getUserRewardModels().stream().mapToInt(UserRewardModel::getRewardPoints).sum();
        List<Provider> providers = tripPricerService.getProvider(user, cumulativeRewardPoints);
        user.setTripDeals(providers);
        return providers;
    }

    /**
     * Get the visited location (add a random visited location)
     *
     * @param user the user model
     * @return the visited location
     */
    public VisitedLocation trackUserLocation(UserModel user) {

        LOGGER.debug("Start track location ");
        VisitedLocation visitedLocation = gpsUtilService.getUserLocation(user.getUserId());

        user.addToVisitedLocations(visitedLocation);
        rewardsService.calculateRewards(user);
        LOGGER.debug("End track location");
        return visitedLocation;
    }

    /**
     * Build user trackers runnable
     * @param users List users
     */
    @SneakyThrows
    public void buildUserTrackThreads(List<UserModel> users) {
        users.forEach(theUser -> {
            Future<?> future = executorService.submit(new TrackUserLocationRunnable(this, theUser));
            futures.add(future);
        });

        LOGGER.debug("###### All tasks are submitted.");
        for (Future<?> future : futures) {
            future.get();
        }
        LOGGER.debug("###### All tasks are completed.");
    }

    /**
     * Get 5 attractions near visited location
     *
     * @param visitedLocation the visited location
     * @param user            the user model
     * @return a list of attractions
     */
    public List<NearByAttractionDTO> getNearByAttractions(VisitedLocation visitedLocation, UserModel user) {

        return gpsUtilService.getAttractions().parallelStream()
                .map(attraction -> createNearByAttractionDTO(attraction, visitedLocation, user))
                .sorted(Comparator.comparingDouble(NearByAttractionDTO::getDistance))
                .limit(5)
                .collect(Collectors.toList());

    }

    /**
     * Create nearby attraction DTO
     * 
     * @param attraction the attraction
     * @param visitedLocation the visited location
     * @param user the user
     * @return the dto of nearbyAttraction
     */
    private NearByAttractionDTO createNearByAttractionDTO(Attraction attraction, VisitedLocation visitedLocation, UserModel user) {
        return new NearByAttractionDTO(attraction.attractionName,
                attraction.latitude,
                attraction.longitude,
                visitedLocation.location.latitude,
                visitedLocation.location.longitude,
                rewardsService.getDistance(
                        new Location(attraction.latitude, attraction.longitude),
                        visitedLocation.location),
                rewardsService.getRewardPoints(attraction, user));
    }

}
