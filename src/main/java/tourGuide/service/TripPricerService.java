package tourGuide.service;

import org.springframework.stereotype.Service;
import tourGuide.constant.Config;
import tourGuide.model.UserModel;
import tripPricer.Provider;
import tripPricer.TripPricer;

import java.util.List;

@Service
public class TripPricerService {

    private static final String API_KEY = Config.TRIP_PRICER_API_KEY;

    private final TripPricer tripPricer;

    public TripPricerService() {
        tripPricer = new TripPricer();
    }

    public List<Provider> getProvider(UserModel user, int cumulativeRewardPoints) {
        return tripPricer.getPrice(API_KEY, user.getUserId(), user.getUserPreferenceModel().getNumberOfAdults(),
                user.getUserPreferenceModel().getNumberOfChildren(), user.getUserPreferenceModel().getTripDuration(), cumulativeRewardPoints);
    }


}
