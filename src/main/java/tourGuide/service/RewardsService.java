package tourGuide.service;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import rewardCentral.RewardCentral;
import tourGuide.constant.Config;
import tourGuide.model.UserModel;
import tourGuide.model.UserRewardModel;
import tourGuide.service.runnable.CalculateRewardRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Service
public class RewardsService {

    private final Logger LOGGER = LoggerFactory.getLogger(RewardsService.class);
    private int proximityBuffer = Config.DEFAULT_PROXIMITY_BUFFER;
    private final GpsUtilService gpsUtilService;
    private final RewardCentral rewardsCentral;
    private final ExecutorService executorService = Executors.newFixedThreadPool(100);
    private final List<Future<?>> futures = new ArrayList<>();


    public RewardsService(GpsUtilService gpsUtilService, RewardCentral rewardCentral) {
        this.gpsUtilService = gpsUtilService;
        this.rewardsCentral = rewardCentral;
    }

    public void setProximityBuffer(int proximityBuffer) {
        this.proximityBuffer = proximityBuffer;
    }

    /**
     * Calculate rewards by attraction with visited location
     *
     * @param user the user model
     */
    public void calculateRewards(UserModel user) {
        List<Attraction> attractions = gpsUtilService.getAttractions();
        CopyOnWriteArrayList<VisitedLocation> userLocations = new CopyOnWriteArrayList<>(user.getVisitedLocations());

        userLocations.forEach(visitedLocation ->
                attractions.parallelStream().forEach(attraction -> {
                    if (userHasNeverVisitedAttraction(user, attraction) && nearAttraction(visitedLocation, attraction)) {
                        user.addUserReward(new UserRewardModel(visitedLocation, attraction, getRewardPoints(attraction, user)));
                    }
                })
        );
    }

    @SneakyThrows
    public void buildCalculateRewardThreads(List<UserModel> users) {
        users.forEach(theUser -> {
            Future<?> future = executorService.submit(new CalculateRewardRunnable(this, theUser));
            futures.add(future);
        });

        LOGGER.debug("###### All tasks are submitted.");
        for (Future<?> future : futures) {
            future.get();
        }
        LOGGER.debug("###### All tasks are completed.");
    }

    /**
     * Check if the attraction is proximity to location
     *
     * @param attraction the attraction
     * @param location   the location
     * @return true if the attraction is proximity to location, otherwise false
     */
    public boolean isWithinAttractionProximity(Attraction attraction, Location location) {
        int attractionProximityRange = Config.DEFAULT_ATTRACTION_PROXIMITY_RANGE;
        return getDistance(attraction, location) < attractionProximityRange;
    }

    /**
     * Check if the attraction is near to visited location
     *
     * @param visitedLocation the visited location
     * @param attraction      the attraction
     * @return true if the attraction is near to visited location, otherwise false
     */
    private boolean nearAttraction(VisitedLocation visitedLocation, Attraction attraction) {
        double distance = getDistance(attraction, visitedLocation.location);
        LOGGER.trace("Nom attraction : " + attraction.attractionName + " - distance : " + distance + " - proximity max : " + proximityBuffer);
        return distance < proximityBuffer;
    }

    /**
     * Create a random reward points
     *
     * @param attraction the attraction
     * @param user       the user
     * @return a reward point
     */
    public int getRewardPoints(Attraction attraction, UserModel user) {
        return rewardsCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId());
    }

    /**
     * Calculate the distance between location 1 and location 2
     *
     * @param loc1 the location 1
     * @param loc2 the location 2
     * @return the distance between location 1 and location 2 per nautical mile
     */
    public double getDistance(Location loc1, Location loc2) {
        double lat1 = Math.toRadians(loc1.latitude);
        double lon1 = Math.toRadians(loc1.longitude);
        double lat2 = Math.toRadians(loc2.latitude);
        double lon2 = Math.toRadians(loc2.longitude);

        double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
                + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

        double nauticalMiles = 60 * Math.toDegrees(angle);

        return Config.STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
    }

    /**
     * Check if user has never visited this attraction
     *
     * @param user       the user
     * @param attraction the attraction
     * @return true if user has never visited this attraction, otherwise false
     */
    private boolean userHasNeverVisitedAttraction(UserModel user, Attraction attraction) {
        return user.getUserRewardModels().stream().noneMatch(
                r -> r.attraction.attractionName.equals(attraction.attractionName)
        );
    }

}
