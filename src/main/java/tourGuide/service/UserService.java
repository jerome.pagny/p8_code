package tourGuide.service;

import org.springframework.stereotype.Service;
import tourGuide.dto.UserPreferenceDTO;
import tourGuide.exception.ResourceNotFoundException;
import tourGuide.model.UserModel;
import tourGuide.model.UserPreferenceModel;
import tourGuide.model.UserTestingModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final UserTestingModel userTestingModel;

    public UserService(UserTestingModel userTestingModel) {
        this.userTestingModel = userTestingModel;
    }


    /**
     * Get a user by username with initializeInternalUsers data
     *
     * @param userName the username
     * @return the user model found in initializeInternalUsers data
     */
    public UserModel find(String userName) throws ResourceNotFoundException {

        Optional<UserModel> user = Optional.ofNullable(userTestingModel.getUsers().get(userName));

        if (!user.isPresent()) {
            throw new ResourceNotFoundException(userName);
        }

        return user.get();
    }

    /**
     * Get all users with initializeInternalUsers data
     *
     * @return a list of user model with initializeInternalUsers data
     */
    public List<UserModel> getAll() {
        return new ArrayList<>(userTestingModel.getUsers().values());
    }

    /**
     * Add a new user model in initializeInternalUsers data
     *
     * @param user the user model
     */
    public void add(UserModel user) {
        if (!userTestingModel.getUsers().containsKey(user.getUserName())) {
            userTestingModel.getUsers().put(user.getUserName(), user);
        }
    }

    public UserPreferenceDTO updatePreference(UserModel user, UserPreferenceDTO userPreferenceToUpdate) {

        UserPreferenceModel preferenceActual = user.getUserPreferenceModel();

        int attractionProximity = userPreferenceToUpdate.getAttractionProximity() != 0
                ? userPreferenceToUpdate.getAttractionProximity()
                : preferenceActual.getAttractionProximity();

        String currency = userPreferenceToUpdate.getCurrency() != null
                ? userPreferenceToUpdate.getCurrency()
                : preferenceActual.getCurrency().getCurrencyCode();

        int lowerPricePoint = userPreferenceToUpdate.getLowerPricePoint() != 0
                ? userPreferenceToUpdate.getLowerPricePoint()
                : preferenceActual.getLowerPricePoint().getContext().getPrecision();

        int highPricePoint = userPreferenceToUpdate.getHighPricePoint() != 0
                ? userPreferenceToUpdate.getHighPricePoint()
                : preferenceActual.getLowerPricePoint().getContext().getMaxScale();

        int tripDuration = userPreferenceToUpdate.getTripDuration() != 0
                ? userPreferenceToUpdate.getTripDuration()
                : preferenceActual.getTripDuration();

        int ticketQuantity = userPreferenceToUpdate.getTicketQuantity() != 0
                ? userPreferenceToUpdate.getTicketQuantity()
                : preferenceActual.getTicketQuantity();

        int numberOfAdults = userPreferenceToUpdate.getNumberOfAdults() != 0
                ? userPreferenceToUpdate.getNumberOfAdults()
                : preferenceActual.getNumberOfAdults();

        int numberOfChildren = userPreferenceToUpdate.getNumberOfChildren() != 0
                ? userPreferenceToUpdate.getNumberOfChildren()
                : preferenceActual.getNumberOfChildren();

        UserPreferenceDTO userPreferenceDTOUpdated = new UserPreferenceDTO(attractionProximity, currency,
                lowerPricePoint,
                highPricePoint,
                tripDuration,
                ticketQuantity,
                numberOfAdults,
                numberOfChildren);

        user.setUserPreferenceModel(new UserPreferenceModel(userPreferenceDTOUpdated));

        return userPreferenceDTOUpdated;
    }

    public UserPreferenceModel getUserPreference(UserModel user) {
        return user.getUserPreferenceModel();
    }

}
