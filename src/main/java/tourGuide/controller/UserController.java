package tourGuide.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import tourGuide.dto.UserPreferenceDTO;
import tourGuide.exception.ResourceNotFoundException;
import tourGuide.model.UserModel;
import tourGuide.model.UserPreferenceModel;
import tourGuide.service.UserService;

@RestController
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/user/preference")
    public UserPreferenceModel getUserPreference(@RequestParam String userName)
            throws ResourceNotFoundException {

        UserModel user = userService.find(userName);

        return userService.getUserPreference(user);
    }

    @PutMapping("/user/preference")
    public UserPreferenceDTO updateUserPreference(@RequestParam String userName, @RequestBody UserPreferenceDTO userPreferenceDTO)
            throws ResourceNotFoundException {

        UserModel user = userService.find(userName);

        return userService.updatePreference(user, userPreferenceDTO);
    }


}
