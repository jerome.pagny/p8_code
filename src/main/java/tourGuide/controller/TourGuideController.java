package tourGuide.controller;

import com.jsoniter.output.JsonStream;
import gpsUtil.location.VisitedLocation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tourGuide.dto.UserLocationDTO;
import tourGuide.exception.ResourceNotFoundException;
import tourGuide.model.UserModel;
import tourGuide.service.TourGuideService;
import tourGuide.service.TrackerService;
import tourGuide.service.UserService;
import tripPricer.Provider;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TourGuideController {

    private final TourGuideService tourGuideService;
    private final UserService userService;
    private final TrackerService trackerService;

    public TourGuideController(TourGuideService tourGuideService, UserService userService, TrackerService trackerService) {
        this.tourGuideService = tourGuideService;
        this.userService = userService;
        this.trackerService = trackerService;
    }


    @RequestMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }

    @RequestMapping("/getLocation")
    public String getLocation(@RequestParam String userName) throws ResourceNotFoundException {
        VisitedLocation visitedLocation = tourGuideService.getUserLocation(userService.find(userName));
        return JsonStream.serialize(visitedLocation.location);
    }

    @RequestMapping("/getNearbyAttractions")
    public String getNearbyAttractions(@RequestParam String userName) throws ResourceNotFoundException {
        UserModel user = userService.find(userName);
        VisitedLocation visitedLocation = tourGuideService.getUserLocation(user);
        return JsonStream.serialize(tourGuideService.getNearByAttractions(visitedLocation, user));
    }

    @RequestMapping("/getRewards")
    public String getRewards(@RequestParam String userName) throws ResourceNotFoundException {
        return JsonStream.serialize(tourGuideService.getUserRewards(userService.find(userName)));
    }

    @RequestMapping("/getAllCurrentLocations")
    public String getAllCurrentLocations() {
        List<UserModel> allUsers = userService.getAll();
        List<UserLocationDTO> listUserLocation = new ArrayList<>();

        allUsers.forEach(user ->
                listUserLocation.add(
                        new UserLocationDTO(
                                user.getUserId().toString(),
                                user.getLastVisitedLocation().location)
                )
        );

        return JsonStream.serialize(listUserLocation);
    }

    @RequestMapping("/getTripDeals")
    public String getTripDeals(@RequestParam String userName) throws ResourceNotFoundException {
        List<Provider> providers = tourGuideService.getTripDeals(userService.find(userName));
        return JsonStream.serialize(providers);
    }

    @RequestMapping("/tracker/start")
    public void startTracking() {
        trackerService.startTracking();
    }

    @RequestMapping("/tracker/stop")
    public void stopTracking() {
        trackerService.stopTracking();
    }

}