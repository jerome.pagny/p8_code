package tourGuide.config;

import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.UserModel;
import tourGuide.model.UserTestingModel;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.IntStream;

@Component
public class BuildUserInternalTesting {

    private static final Logger LOGGER = LoggerFactory.getLogger(BuildUserInternalTesting.class);

    public static UserTestingModel build() {
        Map<String, UserModel> internalUserMap = new HashMap<>();
        IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
            String userName = "internalUser" + i;
            String phone = "000";
            String email = userName + "@tourGuide.com";
            UserModel user = new UserModel(UUID.randomUUID(), userName, phone, email);
            generateUserLocationHistory(user);

            internalUserMap.put(userName, user);
            LOGGER.debug("Build " + userName);
        });
        LOGGER.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
        return new UserTestingModel(internalUserMap);
    }

    private static void generateUserLocationHistory(UserModel user) {
        IntStream.range(0, 3).forEach(i -> user.addToVisitedLocations(
                new VisitedLocation(
                        user.getUserId(),
                        new Location(generateRandomLatitude(),
                                generateRandomLongitude()),
                        getRandomTime()
                )
        ));
    }

    private static double generateRandomLongitude() {
        double leftLimit = -180;
        double rightLimit = 180;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    private static double generateRandomLatitude() {
        double leftLimit = -85.05112878;
        double rightLimit = 85.05112878;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    private static Date getRandomTime() {
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
        return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
    }
}
