package tourGuide.service;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.UserModel;
import tourGuide.model.UserRewardModel;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RewardsServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private GpsUtilService gpsUtilService;

    @Autowired
    private TripPricerService tripPricerService;

    @Autowired
    private TrackerService trackerService;

    @Before
    public void init() {
        Locale.setDefault(Locale.US);

    }

    @Test
    public void userGetRewards() {
        RewardsService rewardsService = new RewardsService(gpsUtilService, new RewardCentral());

        InternalTestHelper.setInternalUserNumber(0);
        trackerService.startTracking();
        TourGuideService tourGuideService = new TourGuideService(gpsUtilService, rewardsService, tripPricerService);

        UserModel user = new UserModel(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        Attraction attraction = gpsUtilService.getAttractions().get(0);
        user.addToVisitedLocations(new VisitedLocation(user.getUserId(), attraction, new Date()));
        tourGuideService.trackUserLocation(user);
        List<UserRewardModel> userRewardModels = user.getUserRewardModels();
        trackerService.stopTracking();

        assertEquals(1, userRewardModels.size());
    }

    @Test
    public void isWithinAttractionProximity() {
        RewardsService rewardsService = new RewardsService(gpsUtilService, new RewardCentral());
        Attraction attraction = gpsUtilService.getAttractions().get(0);
        assertTrue(rewardsService.isWithinAttractionProximity(attraction, attraction));
    }

    @Test
    public void nearAllAttractions() {

        InternalTestHelper.setInternalUserNumber(1);

        RewardsService rewardsService = new RewardsService(gpsUtilService, new RewardCentral());
        rewardsService.setProximityBuffer(Integer.MAX_VALUE);
        trackerService.startTracking();
        TourGuideService tourGuideService = new TourGuideService(gpsUtilService, rewardsService, tripPricerService);

        UserModel user = userService.getAll().get(0);

        rewardsService.calculateRewards(user);

        List<UserRewardModel> userRewardModels = tourGuideService.getUserRewards(user);
        trackerService.stopTracking();

        assertEquals(gpsUtilService.getAttractions().size(), userRewardModels.size());
    }

}
