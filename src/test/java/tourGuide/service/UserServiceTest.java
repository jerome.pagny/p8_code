package tourGuide.service;

import org.javamoney.moneta.Money;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;
import tourGuide.dto.UserPreferenceDTO;
import tourGuide.exception.ResourceNotFoundException;
import tourGuide.model.UserModel;
import tourGuide.model.UserPreferenceModel;
import tourGuide.model.UserTestingModel;

import javax.money.Monetary;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTest {

    private UserService userService;
    @SpyBean
    private UserTestingModel userTestingModel;


    @Before
    public void init() {
        Locale.setDefault(Locale.US);

        userTestingModel.getUsers().clear();
        userTestingModel.getUsers().put("Jean-Luc", new UserModel(UUID.randomUUID(), "Jean-Luc", "0706050403", "jl@mail.com"));
        userTestingModel.getUsers().put("Jean-Marc", new UserModel(UUID.randomUUID(), "Jean-Marc", "0703040506", "jm@mail.com"));

        userService = new UserService(userTestingModel);
    }

    @Test
    public void findAnUser() throws ResourceNotFoundException {
        userService.getAll().forEach(u -> System.out.println(u.getUserName()));

        UserModel user = userService.find("Jean-Luc");

        assertNotNull(user);
        assertEquals("Jean-Luc", user.getUserName());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldBeException_when_userDoesntExist() throws ResourceNotFoundException {
        userService.getAll().forEach(u -> System.out.println(u.getUserName()));
        userService.find("Hidden");
    }

    @Test
    public void listAllUsers() {
        List<UserModel> users = userService.getAll();

        assertEquals(2, users.size());
    }

    @Test
    public void addANewUser() throws ResourceNotFoundException {
        UserModel newUser = new UserModel(UUID.randomUUID(), "Hidden", "xxxxxxxx", "hidden@mail.com");
        userService.add(newUser);

        UserModel userModelFind = userService.find("Hidden");

        assertNotNull(userModelFind);
        assertEquals("Hidden", userModelFind.getUserName());
    }

    @Test
    public void should_beNotAddNewUser_when_userIsAlreadyInserted() throws ResourceNotFoundException {
        UserModel newUser = new UserModel(UUID.randomUUID(), "Jean-Luc", "xxxxx", "jl2@mail.com");
        userService.add(newUser);

        UserModel userModelFind = userService.find("Jean-Luc");

        assertNotNull(userModelFind);
        assertNotEquals("jl2@mail.com", userModelFind.getEmailAddress());
    }

    @Test
    public void showUserPreference() throws ResourceNotFoundException {
        UserModel user = userService.find("Jean-Luc");

        UserPreferenceModel userPreference = userService.getUserPreference(user);

        assertNotNull(userPreference);
        assertEquals(Integer.MAX_VALUE, userPreference.getAttractionProximity());
    }

    @Test
    public void updateUserPreference() throws ResourceNotFoundException {
        UserModel user = userService.find("Jean-Luc");
        UserPreferenceModel userPreference = userService.getUserPreference(user);

        userPreference.setAttractionProximity(1);
        userPreference.setCurrency(Monetary.getCurrency("EUR"));
        userPreference.setLowerPricePoint(Money.of(3, userPreference.getCurrency()));
        userPreference.setHighPricePoint(Money.of(10, userPreference.getCurrency()));
        userPreference.setTripDuration(200);
        userPreference.setTicketQuantity(10);
        userPreference.setNumberOfChildren(10);
        userPreference.setNumberOfAdults(2);

        UserPreferenceDTO userPreferenceDTOToUpdate = new UserPreferenceDTO(userPreference);

        UserPreferenceDTO userPreferenceDTOUpdated = userService.updatePreference(user, userPreferenceDTOToUpdate);

        assertNotNull(userPreferenceDTOUpdated);
        assertEquals(1, userPreferenceDTOUpdated.getAttractionProximity());
    }


}



