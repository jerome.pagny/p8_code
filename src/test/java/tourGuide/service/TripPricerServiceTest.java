package tourGuide.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;
import tourGuide.exception.ResourceNotFoundException;
import tourGuide.model.UserModel;
import tourGuide.model.UserTestingModel;
import tripPricer.Provider;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TripPricerServiceTest {


    @Autowired
    private TripPricerService tripPricerService;

    @Autowired
    private UserService userService;

    @SpyBean
    private UserTestingModel userTestingModel;

    @Before
    public void init() {
        userTestingModel.getUsers().clear();
        userTestingModel.getUsers().put("Jean-Luc", new UserModel(UUID.randomUUID(), "Jean-Luc", "0706050403", "jl@mail.com"));
    }

    @Test
    public void showListProviders() throws ResourceNotFoundException {
        UserModel user = userService.find("Jean-Luc");

        List<Provider> listProviders = tripPricerService.getProvider(user, 1554);

        assertNotNull(listProviders);
        assertTrue(listProviders.size() > 0);
    }


}
