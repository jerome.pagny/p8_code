package tourGuide.service;

import gpsUtil.location.VisitedLocation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;
import rewardCentral.RewardCentral;
import tourGuide.dto.NearByAttractionDTO;
import tourGuide.exception.ResourceNotFoundException;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.UserModel;
import tourGuide.model.UserTestingModel;
import tripPricer.Provider;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TourGuideServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private GpsUtilService gpsUtilService;

    @Autowired
    private TripPricerService tripPricerService;

    @Autowired
    private TrackerService trackerService;

    @SpyBean
    private UserTestingModel userTestingModel;


    @Before
    public void init() {
        Locale.setDefault(Locale.US);

        userTestingModel.getUsers().clear();
        userTestingModel.getUsers().put("1", new UserModel(UUID.randomUUID(), "Jean-Luc", "0706050403", "jl@mail.com"));
        userTestingModel.getUsers().put("2", new UserModel(UUID.randomUUID(), "Jean-Marc", "0703040506", "jm@mail.com"));
    }

    @Test
    public void getUserLocation() {

        InternalTestHelper.setInternalUserNumber(0);

        RewardsService rewardsService = new RewardsService(gpsUtilService, new RewardCentral());
        trackerService.startTracking();
        TourGuideService tourGuideService = new TourGuideService(gpsUtilService, rewardsService, tripPricerService);

        UserModel user = new UserModel(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user);
        trackerService.stopTracking();
        assertEquals(visitedLocation.userId, user.getUserId());
    }

    @Test
    public void addUser() throws ResourceNotFoundException {
        InternalTestHelper.setInternalUserNumber(0);
        trackerService.startTracking();

        UserModel user = new UserModel(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        UserModel user2 = new UserModel(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

        userService.add(user);
        userService.add(user2);

        UserModel retrievedUser = userService.find(user.getUserName());
        UserModel retrievedUser2 = userService.find(user2.getUserName());

        trackerService.stopTracking();

        assertEquals(user, retrievedUser);
        assertEquals(user2, retrievedUser2);
    }

    @Test
    public void getAllUsers() {
        InternalTestHelper.setInternalUserNumber(0);
        trackerService.startTracking();

        UserModel user = new UserModel(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        UserModel user2 = new UserModel(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

        userService.add(user);
        userService.add(user2);

        List<UserModel> allUsers = userService.getAll();

        trackerService.stopTracking();

        assertTrue(allUsers.contains(user));
        assertTrue(allUsers.contains(user2));
    }

    @Test
    public void trackUser() {
        RewardsService rewardsService = new RewardsService(gpsUtilService, new RewardCentral());
        InternalTestHelper.setInternalUserNumber(0);
        trackerService.startTracking();
        TourGuideService tourGuideService = new TourGuideService(gpsUtilService, rewardsService, tripPricerService);

        UserModel user = new UserModel(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user);

        trackerService.stopTracking();

        assertEquals(user.getUserId(), visitedLocation.userId);
    }

    @Test
    public void getNearbyAttractions() {
        RewardsService rewardsService = new RewardsService(gpsUtilService, new RewardCentral());
        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtilService, rewardsService, tripPricerService);

        UserModel user = new UserModel(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user);

        List<NearByAttractionDTO> attractions = tourGuideService.getNearByAttractions(visitedLocation, user);

        assertEquals(5, attractions.size());
    }


    @Test
    public void getTripDeals() {
        RewardsService rewardsService = new RewardsService(gpsUtilService, new RewardCentral());
        InternalTestHelper.setInternalUserNumber(0);
        trackerService.startTracking();
        TourGuideService tourGuideService = new TourGuideService(gpsUtilService, rewardsService, tripPricerService);

        UserModel user = new UserModel(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");

        List<Provider> providers = tourGuideService.getTripDeals(user);

        trackerService.stopTracking();

        assertEquals(5, providers.size());
    }


}
