package tourGuide.service;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;
import tourGuide.exception.ResourceNotFoundException;
import tourGuide.model.UserModel;
import tourGuide.model.UserTestingModel;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class GpsUtilServiceTest {

    @Autowired
    private GpsUtilService gpsUtilService;

    @Autowired
    private UserService userService;

    @SpyBean
    private UserTestingModel userTestingModel;

    @Before
    public void init() {
        Locale.setDefault(Locale.US);
        userTestingModel.getUsers().clear();
        userTestingModel.getUsers().put("Jean-Luc", new UserModel(UUID.randomUUID(), "Jean-Luc", "0706050403", "jl@mail.com"));
    }

    @Test
    public void showListAttractions() {
        List<Attraction> list = gpsUtilService.getAttractions();

        assertNotNull(list);
        assertTrue(list.size() > 0);
    }

    @Test
    public void showLocationFromAnUser() throws ResourceNotFoundException {
        UserModel user = userService.find("Jean-Luc");

        VisitedLocation visitedLocation = gpsUtilService.getUserLocation(user.getUserId());

        assertNotNull(visitedLocation);
    }


}
