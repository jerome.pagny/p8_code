package tourGuide.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import tourGuide.service.UserService;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertNotNull;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest()
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserControllerTestIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webContext;

    @Autowired
    private UserService userService;

    @Before
    public void setupMockMvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webContext).build();
    }

    @Test
    public void showPreferenceFromAnUser() throws Exception {
        mockMvc.perform(get("/user/preference")
                        .param("userName", "internalUser0"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("USD")));
    }

    @Test
    public void updatePreferenceFromAnUser() throws Exception {

        String jsonToUpdate = "{\n" +
                "    \"attractionProximity\": 2147483647,\n" +
                "    \"currency\": \"EUR\",\n" +
                "    \"lowerPricePoint\": 1,\n" +
                "    \"highPricePoint\": 2,\n" +
                "    \"tripDuration\": 1,\n" +
                "    \"ticketQuantity\": 1,\n" +
                "    \"numberOfAdults\": 1,\n" +
                "    \"numberOfChildren\": 0\n" +
                "}";

        mockMvc.perform(put("/user/preference")
                        .param("userName", "internalUser0")
                        .contentType(APPLICATION_JSON)
                        .content(jsonToUpdate)
                        .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("EUR")));
    }

    @Test
    public void shouldBeThrownException_when_userPreferenceDTOToUpdateIsEmpty() throws Exception {
        mockMvc.perform(put("/user/preference")
                        .param("userName", "internalUser0")
                        .contentType(APPLICATION_JSON)
                        .content("")
                        .accept(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertNotNull(result.getResolvedException()))
                .andExpect(result -> content().string(containsString("Required request body is missing")));
    }


}
