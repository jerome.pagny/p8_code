package tourGuide.controller;

import com.jsoniter.output.JsonStream;
import gpsUtil.location.VisitedLocation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import tourGuide.dto.NearByAttractionDTO;
import tourGuide.model.UserModel;
import tourGuide.service.TourGuideService;
import tourGuide.service.TrackerService;
import tourGuide.service.UserService;

import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest()
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TourGuideControllerTestIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webContext;

    @Autowired
    private UserService userService;

    @Autowired
    private TourGuideService tourGuideService;

    @Autowired
    private TrackerService trackerService;

    @Before
    public void setupMockMvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webContext).build();
        trackerService.startTracking();
    }

    @After
    public void setupEnd(){
        trackerService.stopTracking();
    }

    @Test
    public void showMessageWelcome() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().string("Greetings from TourGuide!"));
    }

    @Test
    public void showLocation() throws Exception {
        UserModel user = userService.find("internalUser0");
        String location = JsonStream.serialize(user.getLastVisitedLocation().location);

        mockMvc.perform(get("/getLocation")
                        .param("userName", "internalUser0"))
                .andExpect(status().isOk())
                .andExpect(content().json(location));
    }

    @Test
    public void showListNearByAttraction() throws Exception {
        UserModel user = userService.find("internalUser0");
        VisitedLocation visitedLocation = tourGuideService.getUserLocation(user);
        List<NearByAttractionDTO> listAttractions = tourGuideService.getNearByAttractions(visitedLocation, user);

        mockMvc.perform(get("/getNearbyAttractions")
                        .param("userName", "internalUser0"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("distance")));

        assertEquals(5, listAttractions.size());
    }

    @Test
    public void showRewards() throws Exception {
        UserModel user = userService.find("internalUser0");
        String listReward = JsonStream.serialize(tourGuideService.getUserRewards(user));

        mockMvc.perform(get("/getRewards")
                        .param("userName", "internalUser0"))
                .andExpect(status().isOk())
                .andExpect(content().string(listReward));
    }

    @Test
    public void showAllCurrentLocation() throws Exception {
        List<UserModel> allUser = userService.getAll();

        mockMvc.perform(get("/getAllCurrentLocations"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(allUser.get(0).getUserId().toString())))
                .andExpect(content().string(containsString("latitude")))
                .andExpect(content().string(containsString("longitude")));
    }

    /*
    @Test
    public void showTripDeals() throws Exception {
        mockMvc.perform(get("/getTripDeals")
                        .param("userName", "internalUser0"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("tripId")));
    }
    */

}
